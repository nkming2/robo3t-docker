# robo3t-docker #
Docker image that run Robo 3T

## Run ##
```cd build && docker-compose up -d && docker attach [NAME]```

## You may want to... ##
- Update Robo 3T
	- Replace ```build/robo3t-*.tar.gz``` with a new one from https://github.com/Studio3T/robomongo/releases
	- Find and replace all instances of the original robo3t-* to the new one in Dockerfile
- Save Robo 3T configs in your HOME dir
	- Modify docker-compose.yml
		- Replace ```source: ../runtime/.3T``` with ```source: $HOME/.3T```
		- Replace ```source: ../runtime/.config/3T``` with ```source: $HOME/.config/3T```
- Change the uid of the container user
	- Edit the line ```RUN useradd -u 1000 --create-home user``` in Dockerfile
